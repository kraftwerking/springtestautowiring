package com.kraftwerking.spring.test;

public class FileWriter implements LogWriter  {

	public void write(String text) {
		System.out.println("Write to a file: " + text);
	}
}
